from sys import argv
from random import uniform, randint


def generate(n, thetas):
    x0 = thetas[0]
    thetas = thetas[1:]
    headers = ['X%d' % (i + 1) for i, _ in enumerate(thetas)] + ['Y']
    data = [headers]
    for i in range(n):
        row = [uniform(-5, 5) for _ in thetas]
        combined = zip(row, thetas)
        y = x0 + reduce(lambda acc, x: acc + x[0] * x[1], combined, 0.0)
        noise = (y / 100.0) * uniform(-1, 1)
        row.append(y + noise)
        data.append(row)
    return data


if __name__ == '__main__':
    n = int(argv[1])
    thetas = [float(x) for x in argv[2:]]
    data = generate(n, thetas)
    file_name = 'sample%d.csv' % randint(1, 100)
    with open(file_name, 'w') as f:
        rows = [','.join([str(x) for x in row]) for row in data]
        string_data = '\n'.join(rows)
        f.write(string_data)