from db import db_cursor
from models import Model
from regression import RegressionEngine
from flask import Flask, jsonify, request


app = Flask(__name__)


def validate_data(raw_data):
    data = raw_data.split('\n')
    if len(data) == 1:
        return None
    data = [row.split(',') for row in data]
    for i, row in enumerate(data[1:]):
        if len(data[i-1]) != len(row):
            return None
    data = [[x.strip() for x in row] for row in data]
    return data


@app.route('/create', methods=['POST'])
def create():
    name = request.json['name']
    raw_data = request.json['data'].strip()
    data = validate_data(raw_data)
    if data is None:
        return jsonify(status='error')
    headers = data[0]
    if request.json['has_headers']:
        data = data[1:]
    else:
        headers = ['COL%d' % (i + 1) for i, _ in enumerate(data[0])]
    usage_info = ['none'] * len(headers)
    thetas = ['none'] * len(headers)
    headers = ','.join(headers)
    data = '\n'.join([','.join(row) for row in data])
    to_insert = (name, headers, Model.to_sql_type(usage_info, 'usage_info'), Model.to_sql_type(thetas, 'thetas'), data)
    print to_insert
    with db_cursor() as cursor:
        cursor.execute('INSERT INTO models VALUES(NULL,?,?,?,?,?)', to_insert)
        return jsonify(status='success', model_id=cursor.lastrowid)


@app.route('/list', methods=['POST'])
def list():
    with db_cursor() as cursor:
        cursor.execute('SELECT model_id, name FROM models')
        fetched = cursor.fetchall()
        models = [{'model_id': x[0], 'name': x[1]} for x in fetched]
        return jsonify(models=models)

@app.route('/get/<int:model_id>', methods=['POST'])
def get(model_id):
    with db_cursor() as cursor:
        print model_id
        cursor.execute('SELECT * FROM models WHERE model_id=?', str(model_id))
        result = cursor.fetchone()
        if result is None:
            return jsonify(status='error')
        model = Model.from_row(result)
        return jsonify(model=model.info)

@app.route('/delete/<int:model_id>', methods=['POST'])
def delete(model_id):
    with db_cursor() as cursor:
        cursor.execute('DELETE FROM models WHERE model_id=?', str(model_id))
        return jsonify(status='success')

@app.route('/update/<int:model_id>', methods=['POST'])
def update(model_id):
    print request.json
    name = request.json['name']
    headers = Model.to_sql_type(request.json['headers'], 'headers')
    usage_info = Model.to_sql_type(request.json['usage_info'], 'usage_info')
    data = request.json['data'].strip()
    with db_cursor() as cursor:
        q = 'UPDATE models SET name=?, headers=?, usage_info=?%s WHERE model_id=%d'
        new_info = (name, headers, usage_info)
        validated_data = validate_data(data)
        if validated_data is not None:
            validated_data = Model.to_sql_type(validated_data, 'data')
            new_info = (name, headers, usage_info, validated_data)
            q = q % (', data=?', model_id)
        else:
            q = q % ('', model_id)
        cursor.execute(q, new_info)
        run_regression(cursor, model_id)
        return jsonify(status='success')

def run_regression(cursor, model_id):
    cursor.execute('SELECT * FROM models WHERE model_id=?', str(model_id))
    result = cursor.fetchone()
    model = Model.from_row(result)
    if not model.can_run():
        return
    engine = RegressionEngine(model)
    thetas = engine.run()
    info = (Model.to_sql_type(thetas, 'thetas'), model_id)
    q = 'UPDATE models SET thetas=? WHERE model_id=?'
    cursor.execute(q, info)
