


class Model:
    def __init__(self, info):
        self.info = info

    def can_run(self):
        has_x = False
        has_y = False
        for x in self.info['usage_info']:
            has_x = has_x or x == 'x'
            has_y = (has_y and x != 'y') or (not has_y and x == 'y')
        return has_x and has_y

    def get_data(self):
        usage = self.info['usage_info']
        def get_vector(row):
            y = None
            vec = []
            for i, x in enumerate(row):
                if usage[i] == 'none':
                    continue
                elif usage[i] == 'y':
                    y = float(x)
                elif usage[i] == 'x':
                    vec.append(float(x))
            vec.insert(0, 1)
            vec.insert(0, y)
            return vec
        return [get_vector(row) for row in self.info['data']]

    @staticmethod
    def to_sql_type(elem, key):
        commas = ['headers', 'usage_info', 'thetas']
        if key == 'data':
            return '\n'.join([','.join([x for x in row]) for row in elem])
        elif key in commas:
            return ','.join(elem)
        else:
            return elem

    @classmethod
    def from_row(cls, row):
        info = {}
        info['model_id'] = row[0]
        info['name'] = row[1]
        info['headers'] = row[2].split(',')
        info['usage_info'] = row[3].split(',')
        info['thetas'] = row[4].split(',')
        info['data'] = [row.split(',') for row in row[5].split('\n')]
        return cls(info)
