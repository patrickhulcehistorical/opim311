import sqlite3
from contextlib import contextmanager


@contextmanager
def db_cursor():
    conn = sqlite3.connect('opim.db')
    cursor = conn.cursor()
    yield cursor
    conn.commit()
    conn.close()

def is_initialized(cursor):
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='models'")
    return cursor.fetchone() is not None

def initialize(cursor):
    columns = ['model_id integer primary key',\
        'name text', \
        'headers text', \
        'usage_info text', \
        'thetas text', \
        'data blob' \
        ]
    create_text = 'CREATE TABLE models(%s);' % ','.join(columns)
    cursor.execute(create_text)
