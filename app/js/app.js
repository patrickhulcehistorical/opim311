var app = angular.module('opim311', ['ngRoute', 'highcharts-ng'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/models/new', {
                templateUrl: 'partials/model_create.html',
                controller: 'NewModelCtrl'
            })
            .when('/models/:modelId', {
                templateUrl: 'partials/model_view.html',
                controller: 'ModelCtrl'
            })
            .otherwise({
                redirectTo : '/models/new'
            });
    }]).filter('startFrom', function() {
        return function(input, start) {
            start = parseInt(start);
            if(!input) return [];
            return input.slice(start);
        }
    });