app.controller('ModelListCtrl', ['$scope', '$routeParams','$http', function($scope, $routeParams, $http) {
    $scope.models = [];

    $scope.getModels = function() {
        $http.post('/list')
            .success(function(resp) {
                $scope.models = resp.models;
            });
    };

    $scope.isActiveModel = function(model) {
        return $routeParams.modelId == model.model_id;
    };

    $scope.getModels();
}]);