app.controller('ModelRegressionCtrl', ['$scope', function ($scope) {
  $scope.columnMapping = [];
  $scope.predictionVars = [];
  $scope.predictedAnswer = '?';
  $scope.chartIndex = 2;
  $scope.chartConfig = {
    options: {
      chart: {
        type: 'line'
      },
      plotOptions: {
        line: {
          enableMouseTracking: false
        }
      }
    },
    series: [
      {
        name: 'Trend',
        type: 'line',
        data: [
          [0, 0],
          [1, 1]
        ],
        marker: {
          enabled: false
        },
        states: {
          hover: {
            lineWidth: 0
          }
        }
      },
      {
        name: 'Observations',
        type: 'scatter',
        data: [1, 2, 3, 4],
        marker: {
          radius: 4
        }
      }
    ],
    title: 'Visualize'
  }

  $scope.calculateMapping = function () {
    var yIndex = -1;

    var newMapping = [];
    _.each($scope.model.usage_info, function (t, index) {
      if (t === 'y') yIndex = index;
      else if (t !== 'none') newMapping.push(index);
    });

    if (yIndex === -1) return;
    $scope.chartIndex = newMapping[0];
    newMapping.splice(0, 0, -1);
    newMapping.splice(0, 0, yIndex);

    var zipped = _.zip(newMapping, $scope.columnMapping);
    var unchanged = _.reduce(zipped, function (acc, elem) {
      return acc && elem[0] === elem[1];
    }, true);

    if (!unchanged) $scope.columnMapping = newMapping;
  };

  $scope.predictAnswer = function () {
    if(!$scope.model.thetas) return 0.0;
    var coeffs = $scope.model.thetas.slice(1);
    var combined = _.zip($scope.predictionVars, coeffs);

    return _.reduce(combined, function (acc, elem) {
      var coeff = elem[0] ? elem[0] : 0.0;
      return acc + coeff * parseFloat(elem[1]);
    }, parseFloat($scope.model.thetas[0]));
  };

  $scope.setChartIndex = function (index) {
    $scope.chartIndex = index;
  };

  $scope.refreshChart = function () {
    var allData = $scope.model.data;
    if (!allData) return;

    var thetaIndex = $scope.columnMapping.indexOf($scope.chartIndex) - 1;
    var slope = parseFloat($scope.model.thetas[thetaIndex]);
    var offset = parseFloat($scope.model.thetas[0]);

    var xData = [];
    var yData = [];

    for (var i = 0; i < allData.length; i++) {
      var row = allData[i];
      xData.push(parseFloat(row[$scope.chartIndex]));
      yData.push(parseFloat(row[$scope.columnMapping[0]]));
    }

    var startX = Math.min(0, _.min(xData));
    var startY = offset + startX * slope;
    var endX = _.max(xData);
    var endY = offset + endX * slope;

    var dataPoints = _.zip(xData, yData);
    var linePoints = [
      [startX, startY],
      [endX, endY]
    ];

    $scope.chartConfig.series[0].data = linePoints;
    $scope.chartConfig.series[1].data = dataPoints;
  };

  $scope.$watch('model.usage_info', function () {
    $scope.calculateMapping();
  });

  $scope.$watch('predictionVars', function () {
    $scope.predictedAnswer = $scope.predictAnswer();
  }, true);

  $scope.$watch('model.data + chartIndex', function () {
    $scope.refreshChart();
  });
}]);