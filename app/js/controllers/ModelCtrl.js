app.controller('ModelCtrl', ['$scope', '$routeParams', '$location', '$http', '$timeout', function ($scope, $routeParams, $location, $http, $timeout) {
  $scope.model = {};
  $scope.newHeaders = [];
  $scope.editMode = false;
  $scope.isLoading = false;
  $scope.usageOptions = ['none', 'x', 'y'];
  $scope.newData = '';
  $scope.runResults = {};

  $scope.stopLoading = function() {
    $timeout(function() {
      $scope.isLoading = false;
    }, 500);
  };

  $scope.getModel = function () {
    $scope.isLoading = true;
    $http.post('/get/' + $routeParams.modelId)
      .success(function (resp) {
        var model = resp.model;
        for (var i = 0; i < $scope.models.length; i++) {
          if ($scope.models[i].model_id != model.model_id) continue;
          $scope.models[i] = model;
          $scope.model = $scope.models[i];
          break;
        }
        var h = [];
        h.push.apply(h, resp.model.headers);
        $scope.newHeaders = h;
        $scope.stopLoading();
      });
  };

  $scope.saveModel = function () {
    $scope.isLoading = true;
    $http.post('/update/' + $routeParams.modelId, {
      name: $scope.model.name,
      headers: $scope.newHeaders,
      usage_info: $scope.model.usage_info,
      data: $scope.newData
    }).success(function (resp) {
      $scope.getModel();
      $scope.editMode = false;
      $scope.stopLoading();
    });
  };

  $scope.deleteModel = function () {
    $http.post('/delete/' + $routeParams.modelId).success(function (resp) {
      var modelIndex = $scope.models.indexOf($scope.model);
      $scope.models.splice(modelIndex, 1);
      $location.path('/models/new');
    });
  }

  $scope.hasPredictionResults = function () {
    return _.reduce($scope.model.thetas, function (acc, th) {
      return acc || th !== 'none';
    }, false);
  };

  $scope.displayUsage = function (type) {
    return {
      none: 'Unused',
      y: 'Goal',
      x: 'Linear Predictor'
    }[type];
  };

  $scope.getModel();
}]);