app.controller('NewModelCtrl', ['$scope', '$location','$http', function($scope, $location, $http) {
    $scope.name = '';
    $scope.data = '';
    $scope.hasHeaders = false;

    $scope.createModel = function() {
        $http.post('/create', {
            name: $scope.name,
            data: $scope.data,
            has_headers: $scope.hasHeaders
        }).success(function(resp) {
            if(resp.status !== 'success') {
                alert('Malformed CSV, please try again.');
                return;
            }
            $scope.getModels();
            $location.path('/models/' + resp.model_id);
        });
    };
}]);