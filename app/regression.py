import numpy as np
from models import Model


class RegressionEngine:
    def __init__(self, model, alpha=0.01):
        raw_data = model.get_data()
        self.n = len(raw_data)
        self.k = len(raw_data[0]) - 1
        self.alpha = alpha
        self.data = np.array(raw_data)

        self.xs = self.data[:,1:]
        self.ys = self.data[:,:1]
        self.thetas = np.random.rand(1, self.k)

        self.current_cost = self.cost()
        self.diff_cost = 1.0
        self.costs = []

        print self.ys
        print self.xs

    def run(self):
        i = 0
        while i < 200:
            self.step()
            print 'Cost is %f on iteration %d' % (self.current_cost, i)
            if self.diff_cost > 0:
                self.alpha = self.alpha / 10
            else:
                self.alpha = self.alpha * 2
            print "Alpha is now %f" % self.alpha
            i += 1
        return [str(x) for x in self.thetas[0]]


    def step(self):
        yhats = self.xs.dot(self.thetas.transpose())
        errors = yhats - self.ys
        residuals = errors.dot(np.ones((1,self.k)))
        print residuals
        d_theta = sum(residuals * self.xs).transpose()
        print -1 * d_theta
        self.thetas = self.thetas - (self.alpha / self.n) * d_theta
        cost = self.cost()
        self.costs.append(self.current_cost)
        self.diff_cost = cost - self.current_cost
        self.current_cost = cost

    def cost(self):
        yhats = self.xs.dot(self.thetas.transpose())
        sse = sum((yhats - self.ys) ** 2)[0]
        return sse / (2 * self.n)
