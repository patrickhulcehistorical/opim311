module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            app: {
                files: {
                    'app/static/css/app.css': ['app/less/styles.less']
                }
            }
        },
        concat: {
            libs: {
                files: {
                    'app/static/css/vendor.css': [
                        'bower_components/bootstrap/dist/css/bootstrap.css'
                    ],
                    'app/static/js/vendor.js': [
                        'bower_components/jquery/dist/jquery.js',
                        'bower_components/angular/angular.js',
                        'bower_components/bootstrap/dist/js/bootstrap.js',
                        'bower_components/angular-bootstrap/ui-bootstrap.js',
                        'bower_components/angular-route/angular-route.js',
                        'bower_components/underscore/underscore.js',
                        'bower_components/highcharts/highcharts.js',
                        'bower_components/highcharts-ng/dist/highcharts-ng.js'
                    ]
                }
            },
            app: {
                files: {
                    'app/static/js/app.js': ['app/js/**/*.js']
                }
            }
        },
        watch: {
            app: {
                files: ['app/js/**/*.js', 'app/less/**/*.less'],
                tasks: ['less','concat:app']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['less', 'concat']);
};