# Regress-o-matic


Web application to easily conduct linear regression on data sets and visualize results.


## Getting Started

### Requirements

- Python 2.7.x
- Node.js
- bower
- grunt

### Setup

#### Install Dependencies
```
$ virtualenv env
$ source env/bin/activate
$ pip install -r requirements.txt
$ npm install
$ bower install
```

#### Build and Run
```
$ grunt
$ python run.py
```

Now visit [http://localhost:5000/static/index.html](http://localhost:5000/static/index.html) in your browser to get visualizing!


### Generating Test Data

Regress-o-matic employs gradient descent to determine the theta values for regression.
While this ensures greater flexibility, it performs poorly on data not already normalized.
To generate ideal data for easy testing there's an included data generation file.
Usage is as follows: ```$ python test/generate.py N t0 t1 t2 ... tJ```
where `N` is the number of cases to generate and `t0` through `tJ` are the desired thetas for features `x0` through `xJ`.



## Workflow
This section steps through what Regress-o-matic does and its intended usage.

### Creating a Model
Creating a model is as easy as entering a title for your model and pasting in your CSV.
Check the box if your data already has headers as the first row or not and click to continue.

### Updating the Settings
Regress-o-matic has now created a model for you with your data but all of the columns are currently unused.
Click edit at the top of the page to select how each column will be used in your regression.
There are three options

- Unused
- Linear Predictor
- Goal

Unused means Regress-o-matic will ignore this column when performing the regression.
Linear Predictor means Regress-o-matic will use this column as a linear predictor.
Goal means Regress-o-matic will attempt to predict this column with the other linear predictor columns.

Once you are happy with your settings, you can give each column a header and click save.
Regress-o-matic will now run the regression with the parameters you specified.
Note that you must have exactly one Goal column and at least one Linear Predictor column for regression to run.
You may find yourself changing these settings frequently to accurately reflect your model.

### Visualize the Data
Regress-o-matic provides a graph with all of the observations along with a trend line for each linear predictor.
You can cycle through the linear predictors by clicking on their headers to the right.


### Assess the Results
Regress-o-matic also provides the determined theta values along with a prediction form.
If the theta values don't seem right, try normalizing your data or changing your settings and running again.
You can use this model to predict new Goal values by entering sample values for each of your linear predictors in the prediction form.


## How It Works

### Gradient Descent
Regress-o-matic employs gradient descent to determine the proper theta values.
Gradient descent is an iterative process for minimizing the cost of a model.
Thetas are randomly initialized, the cost function is computed, the derivative with respect to the thetas is found, and the thetas are adjusted by a learning rate alpha.
This process is repeated until the cost reaches some minimum threshold or the maximum number of iterations is reached.
The cost function in our case is simply the SSE of the predicted set from the true Goal value.


### Modifications
Gradient descent is likely to perform suboptimally unless the data is normalized to fall within the same general range.
To help compensate for this and allow Regress-o-matic to perform on a wider range of data sets, the learning rate alpha had to be adjusted in the regression process.
Normally, a human manually tweaks the learning rate to find the best results.
In Regress-o-matic, the learning rate is dynamic and learns about the data set as the regression is being run.
If the cost begins growing, the learning rate is scaled back, if the cost is decreasing very slowly the learning rate is scaled up.



## Takeaways

#### Gradient descent is difficult to automate on wide ranges of data
Gradient descent is a wonderfully flexible tool to be used on all kinds of data associations, but generally requires a certain amount of preprocessing.
It's difficult to strike a balance of leaving the data in a clearly recognizable format compared to the original data set, but still obtain meaningful associations.

#### Generic data analysis isn't possible without AI
It's difficult to quantify and delineate all of the considerations made when a person determines the associativeness of two data points.
Even when constricting this very limited project to just linear regression and gradient descent, problems still arose with choosing the wrong alpha value.
The learning rate that learns was a primitive artificial intelligence construct used to bridge this gap.

#### Regress-o-matic is a quick visualization tool, not a replacement for analytics
If OPIM311 has shown anything, it's that the use of programming to find patterns does not eliminate the need for a human element.
Regress-o-matic is helpful in quickly visualizing the data and finding very simple associations between variables, but still requires a human operator to explore further.
