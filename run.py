from app import db
from app.app import app as flask_app


with db.db_cursor() as cursor:
    if not db.is_initialized(cursor):
        db.initialize(cursor)

flask_app.run(debug=True)
